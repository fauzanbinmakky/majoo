import 'dart:io';

Future<bool> cekInternet() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return Future.value(true);
    } else {
      return true;
    }
  } on SocketException catch (_) {
    return Future.value(false);
  }
}
