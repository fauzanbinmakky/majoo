import 'dart:async';

import 'package:floor/floor.dart';
import 'package:test_majoo/local/film_dao.dart';
import 'package:test_majoo/local/local_film.dart';
import 'package:test_majoo/local/local_people.dart';
import 'package:test_majoo/local/local_spesies.dart';
import 'package:test_majoo/local/local_user.dart';
import 'package:test_majoo/local/people_dao.dart';
import 'package:test_majoo/local/spesies_dao.dart';
import 'package:test_majoo/local/user_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'appdatabase.g.dart';

@Database(
    version: 1, entities: [LocalUser, LocalPeople, LocalFilm, LocalSpecies])
abstract class AppDatabase extends FloorDatabase {
  UserDao get userDao;

  PeopleDao get peoppleDao;

  FilmDao get filmDao;

  SpesiesDao get spesiesDao;
}
