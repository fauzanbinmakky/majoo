import 'dart:ui';

class ColorHelper {
  static Color primary = const Color(0xff005e6a);
  static Color darkprimary = const Color(0xff1E4C56);
  static Color bggrey = const Color(0xfff7f8fa);
  static Color linegrey = const Color(0xffd4dce1);
  static Color white = const Color(0xfff5f6fa);
  static Color textsecondary = const Color(0xff939393);
  static Color orangetua = const Color(0xffff7700);
  static Color orangemuda = const Color(0xffffaa00);
  static int absenMasuk = 0xff91d536;
  static int absenSakit = 0xfffec200;
  static int absenIzin = 0xff3885E0;
  static int absenBelum = 0xff9a9a9a;
  static int absenAlpha = 0xfffd3e45;
  static int colorSelected = 0xff004c56;
  static int colorwhite = 0xfff5f6fa;
  static int colorwblack = 0xff000000;
  static int colorbtnnonactive = 0xff9a9a9a;
  static int colortextSeocndary = 0xff939393;

}