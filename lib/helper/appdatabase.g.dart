// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appdatabase.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  UserDao? _userDaoInstance;

  PeopleDao? _peoppleDaoInstance;

  FilmDao? _filmDaoInstance;

  SpesiesDao? _spesiesDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `LocalUser` (`userid` TEXT NOT NULL, `nama` TEXT NOT NULL, `password` TEXT NOT NULL, `alamat` TEXT NOT NULL, `gender` TEXT NOT NULL, `token` TEXT NOT NULL, PRIMARY KEY (`userid`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `LocalPeople` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `height` TEXT NOT NULL, `mass` TEXT NOT NULL, `hair_color` TEXT NOT NULL, `skin_color` TEXT NOT NULL, `eye_color` TEXT NOT NULL, `birth_year` TEXT NOT NULL, `gender` TEXT NOT NULL, `favorit` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `LocalFilm` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idPeople` INTEGER NOT NULL, `judul` TEXT NOT NULL, `film` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `LocalSpecies` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idPeople` INTEGER NOT NULL, `judul` TEXT NOT NULL, `species` TEXT NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  UserDao get userDao {
    return _userDaoInstance ??= _$UserDao(database, changeListener);
  }

  @override
  PeopleDao get peoppleDao {
    return _peoppleDaoInstance ??= _$PeopleDao(database, changeListener);
  }

  @override
  FilmDao get filmDao {
    return _filmDaoInstance ??= _$FilmDao(database, changeListener);
  }

  @override
  SpesiesDao get spesiesDao {
    return _spesiesDaoInstance ??= _$SpesiesDao(database, changeListener);
  }
}

class _$UserDao extends UserDao {
  _$UserDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _localUserInsertionAdapter = InsertionAdapter(
            database,
            'LocalUser',
            (LocalUser item) => <String, Object?>{
                  'userid': item.userid,
                  'nama': item.nama,
                  'password': item.password,
                  'alamat': item.alamat,
                  'gender': item.gender,
                  'token': item.token
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<LocalUser> _localUserInsertionAdapter;

  @override
  Future<LocalUser?> findUseron(String token) async {
    return _queryAdapter.query('SELECT * FROM LocalUser WHERE token =?1',
        mapper: (Map<String, Object?> row) => LocalUser(
            row['userid'] as String,
            row['nama'] as String,
            row['password'] as String,
            row['alamat'] as String,
            row['gender'] as String,
            row['token'] as String),
        arguments: [token]);
  }

  @override
  Future<LocalUser?> cekUserId(String userid) async {
    return _queryAdapter.query('SELECT * FROM LocalUser WHERE userid =?1',
        mapper: (Map<String, Object?> row) => LocalUser(
            row['userid'] as String,
            row['nama'] as String,
            row['password'] as String,
            row['alamat'] as String,
            row['gender'] as String,
            row['token'] as String),
        arguments: [userid]);
  }

  @override
  Future<LocalUser?> cekLogin(String userid, String password) async {
    return _queryAdapter.query(
        'SELECT * FROM LocalUser WHERE userid =?1 and password=?2',
        mapper: (Map<String, Object?> row) => LocalUser(
            row['userid'] as String,
            row['nama'] as String,
            row['password'] as String,
            row['alamat'] as String,
            row['gender'] as String,
            row['token'] as String),
        arguments: [userid, password]);
  }

  @override
  Future<void> deleteAllUser() async {
    await _queryAdapter.queryNoReturn('DELETE FROM LocalUser');
  }

  @override
  Future<void> deleteAllUserbyToken(String token) async {
    await _queryAdapter.queryNoReturn('DELETE FROM LocalUser where token =?1',
        arguments: [token]);
  }

  @override
  Future<void> insertUser(LocalUser user) async {
    await _localUserInsertionAdapter.insert(user, OnConflictStrategy.abort);
  }
}

class _$PeopleDao extends PeopleDao {
  _$PeopleDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _localPeopleInsertionAdapter = InsertionAdapter(
            database,
            'LocalPeople',
            (LocalPeople item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'height': item.height,
                  'mass': item.mass,
                  'hair_color': item.hair_color,
                  'skin_color': item.skin_color,
                  'eye_color': item.eye_color,
                  'birth_year': item.birth_year,
                  'gender': item.gender,
                  'favorit': item.favorit
                }),
        _localPeopleUpdateAdapter = UpdateAdapter(
            database,
            'LocalPeople',
            ['id'],
            (LocalPeople item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'height': item.height,
                  'mass': item.mass,
                  'hair_color': item.hair_color,
                  'skin_color': item.skin_color,
                  'eye_color': item.eye_color,
                  'birth_year': item.birth_year,
                  'gender': item.gender,
                  'favorit': item.favorit
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<LocalPeople> _localPeopleInsertionAdapter;

  final UpdateAdapter<LocalPeople> _localPeopleUpdateAdapter;

  @override
  Future<List<LocalPeople?>> findPeoppleAsc() async {
    return _queryAdapter.queryList('SELECT * FROM LocalPeople order by id asc',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String));
  }

  @override
  Future<LocalPeople?> findPeoplebyId(int id) async {
    return _queryAdapter.query('SELECT * FROM LocalPeople WHERE id =?1 limit 1',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String),
        arguments: [id]);
  }

  @override
  Future<List<LocalPeople?>> findPeoppleDesc() async {
    return _queryAdapter.queryList('SELECT * FROM LocalPeople order by id desc',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String));
  }

  @override
  Future<List<LocalPeople?>> findPeoppleall() async {
    return _queryAdapter.queryList('SELECT * FROM LocalPeople',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String));
  }

  @override
  Future<LocalPeople?> findlastId() async {
    return _queryAdapter.query(
        'SELECT * FROM LocalPeople order by id desc limit 1',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String));
  }

  @override
  Future<List<LocalPeople?>> findPeopleby(String kunci) async {
    return _queryAdapter.queryList(
        'SELECT * FROM LocalPeople where name LIKE ?1  or (height LIKE ?1) or (mass LIKE ?1) or (hair_color LIKE ?1) or (skin_color LIKE ?1) or (eye_color LIKE ?1) or (birth_year LIKE ?1) or (gender LIKE ?1) order by id asc',
        mapper: (Map<String, Object?> row) => LocalPeople(row['id'] as int, row['name'] as String, row['height'] as String, row['mass'] as String, row['hair_color'] as String, row['skin_color'] as String, row['eye_color'] as String, row['birth_year'] as String, row['gender'] as String, row['favorit'] as String),
        arguments: [kunci]);
  }

  @override
  Future<List<LocalPeople?>> findfavoritPeople() async {
    return _queryAdapter.queryList(
        'SELECT * FROM LocalPeople WHERE favorit =\"1\"',
        mapper: (Map<String, Object?> row) => LocalPeople(
            row['id'] as int,
            row['name'] as String,
            row['height'] as String,
            row['mass'] as String,
            row['hair_color'] as String,
            row['skin_color'] as String,
            row['eye_color'] as String,
            row['birth_year'] as String,
            row['gender'] as String,
            row['favorit'] as String));
  }

  @override
  Future<void> deleteAllPeople() async {
    await _queryAdapter.queryNoReturn('DELETE FROM LocalPeople');
  }

  @override
  Future<void> deleteAllUserbyid(int id) async {
    await _queryAdapter
        .queryNoReturn('DELETE FROM LocalPeople where id =?1', arguments: [id]);
  }

  @override
  Future<void> uddateDatapeople(
      int id,
      String name,
      String height,
      String mass,
      String hair_color,
      String skin_color,
      String eye_color,
      String birth_year,
      String gender) async {
    await _queryAdapter.queryNoReturn(
        'Update LocalPeople SET name= ?2 ,height= ?3, mass=?4,hair_color =?5,skin_color =?6,eye_color =?7, birth_year =?8, gender=?9 WHERE id =?1',
        arguments: [
          id,
          name,
          height,
          mass,
          hair_color,
          skin_color,
          eye_color,
          birth_year,
          gender
        ]);
  }

  @override
  Future<void> uddateFavoritpeople(int id, String favorit) async {
    await _queryAdapter.queryNoReturn(
        'Update LocalPeople SET favorit =?2  WHERE id =?1',
        arguments: [id, favorit]);
  }

  @override
  Future<void> insertPeople(LocalPeople people) async {
    await _localPeopleInsertionAdapter.insert(people, OnConflictStrategy.abort);
  }

  @override
  Future<void> updatePeople(LocalPeople people) async {
    await _localPeopleUpdateAdapter.update(people, OnConflictStrategy.abort);
  }
}

class _$FilmDao extends FilmDao {
  _$FilmDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _localFilmInsertionAdapter = InsertionAdapter(
            database,
            'LocalFilm',
            (LocalFilm item) => <String, Object?>{
                  'id': item.id,
                  'idPeople': item.idPeople,
                  'judul': item.judul,
                  'film': item.film
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<LocalFilm> _localFilmInsertionAdapter;

  @override
  Future<List<LocalFilm?>> findfilmbyId(int id) async {
    return _queryAdapter.queryList('SELECT * FROM LocalFilm WHERE idPeople =?1',
        mapper: (Map<String, Object?> row) => LocalFilm(
            row['id'] as int,
            row['idPeople'] as int,
            row['judul'] as String,
            row['film'] as String),
        arguments: [id]);
  }

  @override
  Future<LocalFilm?> findFilmlastId() async {
    return _queryAdapter.query(
        'SELECT * FROM LocalFilm order by id desc limit 1',
        mapper: (Map<String, Object?> row) => LocalFilm(
            row['id'] as int,
            row['idPeople'] as int,
            row['judul'] as String,
            row['film'] as String));
  }

  @override
  Future<void> insertUser(LocalFilm film) async {
    await _localFilmInsertionAdapter.insert(film, OnConflictStrategy.abort);
  }
}

class _$SpesiesDao extends SpesiesDao {
  _$SpesiesDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _localSpeciesInsertionAdapter = InsertionAdapter(
            database,
            'LocalSpecies',
            (LocalSpecies item) => <String, Object?>{
                  'id': item.id,
                  'idPeople': item.idPeople,
                  'judul': item.judul,
                  'species': item.species
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<LocalSpecies> _localSpeciesInsertionAdapter;

  @override
  Future<List<LocalSpecies?>> findSpesiesbyId(int id) async {
    return _queryAdapter.queryList(
        'SELECT * FROM LocalSpecies WHERE idPeople =?1',
        mapper: (Map<String, Object?> row) => LocalSpecies(
            row['id'] as int,
            row['idPeople'] as int,
            row['judul'] as String,
            row['species'] as String),
        arguments: [id]);
  }

  @override
  Future<LocalSpecies?> findSpecieslastId() async {
    return _queryAdapter.query(
        'SELECT * FROM LocalSpecies order by id desc limit 1',
        mapper: (Map<String, Object?> row) => LocalSpecies(
            row['id'] as int,
            row['idPeople'] as int,
            row['judul'] as String,
            row['species'] as String));
  }

  @override
  Future<void> insertSpesies(LocalSpecies species) async {
    await _localSpeciesInsertionAdapter.insert(
        species, OnConflictStrategy.abort);
  }
}
