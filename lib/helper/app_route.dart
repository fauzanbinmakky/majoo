import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/addpeaople/add_peaople_bloc.dart';
import 'package:test_majoo/bloc/auth/auth_bloc.dart';
import 'package:test_majoo/bloc/auth/auth_event.dart';
import 'package:test_majoo/bloc/beranda/favorit_bloc.dart';
import 'package:test_majoo/bloc/beranda/navbar_bloc.dart';
import 'package:test_majoo/bloc/beranda/people_bloc.dart';
import 'package:test_majoo/bloc/beranda/search_bloc.dart';
import 'package:test_majoo/bloc/beranda/sort_bloc.dart';
import 'package:test_majoo/bloc/beranda/view_bloc.dart';
import 'package:test_majoo/bloc/detail_people/film_bloc.dart';
import 'package:test_majoo/bloc/login/login_bloc.dart';
import 'package:test_majoo/bloc/login/show_password_bloc.dart';
import 'package:test_majoo/bloc/profil/profil_bloc.dart';
import 'package:test_majoo/bloc/profil/profil_event.dart';
import 'package:test_majoo/bloc/register/register_bloc.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_bloc.dart';
import 'package:test_majoo/repositiry/film_repo.dart';
import 'package:test_majoo/repositiry/people_repo.dart';
import 'package:test_majoo/repositiry/user_repo.dart';
import 'package:test_majoo/ui/add_people.dart';
import 'package:test_majoo/ui/dashboard.dart';
import 'package:test_majoo/ui/detail_profile.dart';
import 'package:test_majoo/ui/edit_people.dart';
import 'package:test_majoo/ui/login.dart';
import 'package:test_majoo/ui/register.dart';
import 'package:test_majoo/ui/splash_screen.dart';

class AppRoute {
  static UserRepoImp _userRepoImp = UserRepoImp();
  static PeopleRepoImp _peopleRepoImp = PeopleRepoImp();
  static FilmRepoImp _filmRepoImp = FilmRepoImp();
  RegisterBloc _registerBloc = RegisterBloc(registerRepoImp: _userRepoImp);
  final AuthBloc _authBloc = AuthBloc();
  final NavBarBloc _navBarBloc = NavBarBloc();
  final LoginBloc _loginBloc =
      LoginBloc(userRepoImp: _userRepoImp, pepopleRepoImp: _peopleRepoImp);
  final ProfilBloc _profilBloc = ProfilBloc(userRepoImp: _userRepoImp);
  final ViewBloc _viewBloc = ViewBloc();

  PeopleBloc _peopleBloc = PeopleBloc(peopleRepoImp: _peopleRepoImp);

  AddPeopleBloc _addPeopleBloc = AddPeopleBloc(peopleRepoImp: _peopleRepoImp);

  UbahPeopleBloc _ubahPeopleBloc =
      UbahPeopleBloc(peopleRepoImp: _peopleRepoImp);

  FavoritBloc _favoritBloc = FavoritBloc(peopleRepoImp: _peopleRepoImp);

  SearchBloc _searchBloc = SearchBloc();

  SortBloc _sortBloc = SortBloc();

  ShowPasswordBloc _showPasswordBloc = ShowPasswordBloc();
  FilmBloc _filmBloc = FilmBloc(repoImp: _filmRepoImp);

  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(
            builder: (_) => BlocProvider.value(
                value: _authBloc..add(CekAuth()), child: SplashScreen()));

      case "/add":
        return MaterialPageRoute(
            builder: (_) =>
                BlocProvider.value(value: _addPeopleBloc, child: AddPeople()));

      case "/edit":
        final args = routeSettings.arguments as EditPeople;
        return MaterialPageRoute(
            builder: (_) => BlocProvider.value(
                value: _ubahPeopleBloc, child: EditPeople(args.localPeople)));

      case "/detail":
        final args = routeSettings.arguments as DetailProfil;
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider.value(value: _ubahPeopleBloc),
                    BlocProvider.value(value: _filmBloc)
                  ],
                  child: DetailProfil(args.localPeople),
                ));

      case "/login":
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider.value(value: _loginBloc),
                  BlocProvider.value(value: _showPasswordBloc)
                ], child: Login()));

      case "/register":
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider.value(value: _registerBloc),
                  BlocProvider.value(value: _showPasswordBloc)
                ], child: Register()));

      case "/home":
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(providers: [
                  BlocProvider.value(value: _navBarBloc),
                  BlocProvider.value(value: _viewBloc),
                  BlocProvider.value(value: _peopleBloc),
                  BlocProvider.value(value: _favoritBloc),
                  BlocProvider.value(value: _searchBloc),
                  BlocProvider.value(value: _sortBloc),
                  BlocProvider.value(value: _profilBloc..add(getProfileOn())),
                ], child: Dashboard()));

      default:
        return MaterialPageRoute(
            builder: (_) => BlocProvider.value(
                value: _authBloc..add(CekAuth()), child: SplashScreen()));
    }
  }

  dispose() {
    _registerBloc.close();
    _authBloc.close();
    _loginBloc.close();
    _navBarBloc.close();
    _profilBloc.close();
    _viewBloc.close();
    _peopleBloc.close();
    _addPeopleBloc.close();
    _ubahPeopleBloc.close();
    _favoritBloc.close();
    _searchBloc.close();
    _sortBloc.close();
    _showPasswordBloc.close();
    _filmBloc.close();
  }
}
