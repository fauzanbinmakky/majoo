import 'dart:io';

class DataHelper {
  static var databasename = "majo_database.db";
  static var toLogin = "/login";
  static var toRegister = "/register";
  static var toHome = "/home";
  static var toSplash = "/splash";
  static var toAdd = "/add";
  static var toDetail = "/detail";
  static var toEdit = "/edit";

}
