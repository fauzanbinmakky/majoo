import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/helper/appdatabase.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/local/local_user.dart';
import 'package:test_majoo/local/user_dao.dart';

abstract class UserRepo {
  Future<String> register(String userid, String nama, String password,
      String alamat, String gender, String token);

  Future<bool> cekuser(String userid);

  Future<String> login(String userid, String password);

  Future<LocalUser?> getUserOn(String token);
}

class UserRepoImp extends UserRepo {
  late UserDao userDao;

  @override
  Future<String> register(String userid, String nama, String password,
      String alamat, String gender, String token) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    userDao = databse.userDao;
    userDao
        .insertUser(LocalUser(userid, nama, password, alamat, gender, token));

    var user = await userDao.findUseron(token);
    if (user!.token == token) {

      return "1";
    } else {
      return "0";
    }
  }

  @override
  Future<bool> cekuser(String userid) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();

    userDao = databse.userDao;
    var data = await userDao.cekUserId(userid);

    if (data != null) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Future<String> login(String userid, String password) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();

    userDao = databse.userDao;
    var data = await userDao.cekLogin(userid, password);

    if (data != null && data.token != "") {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("token", data.token);
      return data.token;
    } else {
      return "";
    }
  }

  @override
  Future<LocalUser?> getUserOn(String token)async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();

    userDao = databse.userDao;
    var data = await userDao.findUseron(token);

    if(data!=null){
      return data;
    }else{
      return null;
    }
  }
}
