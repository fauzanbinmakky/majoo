import 'package:test_majoo/helper/appdatabase.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/local/film_dao.dart';
import 'package:test_majoo/local/local_film.dart';

abstract class FilmRepo {
  Future<List<LocalFilm?>> getFilm(int id);
}

class FilmRepoImp extends FilmRepo {
  late FilmDao filmDao;

  @override
  Future<List<LocalFilm?>> getFilm(int id) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    filmDao = databse.filmDao;
    var data = await filmDao.findfilmbyId(id);
    if (data.isNotEmpty) {
      return data;
    } else {
      return [];
    }
  }
}
