import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/helper/appdatabase.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/local/film_dao.dart';
import 'package:test_majoo/local/local_film.dart';
import 'package:test_majoo/local/local_people.dart';
import 'package:test_majoo/local/local_spesies.dart';
import 'package:test_majoo/local/people_dao.dart';
import 'package:test_majoo/local/spesies_dao.dart';
import 'package:test_majoo/model/people.dart';

abstract class PeopleRepo {
  Future<List<LocalPeople?>> getLocalAllPeople();

  Future<List<LocalPeople?>> getLocalFavoritPeople();

  Future<List<LocalPeople?>> getLocalAllPeopleSort(String tipe);

  Future<List<LocalPeople?>> getPeopple();

  Future<String> AddPeopple(
      String name,
      String height,
      String mass,
      String hainColor,
      String skincolor,
      String eyeColor,
      String brithyear,
      String gender);

  Future<LocalPeople?> EditPeopple(
      int id,
      String name,
      String height,
      String mass,
      String hainColor,
      String skincolor,
      String eyeColor,
      String brithyear,
      String gender);

  Future<String> HapusPeopple(
    int id,
  );

  Future<String> FavoritPeopple(int id, String favorit);

  Future<List<LocalPeople?>> getPeoppleSearch(String kunci, String tipe);
}

class PeopleRepoImp extends PeopleRepo {
  late PeopleDao peopleDao;
  late FilmDao filmDao;
  late SpesiesDao spesiesDao;

  @override
  Future<List<LocalPeople?>> getLocalAllPeople() async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;

    var data = await peopleDao.findPeoppleall();
    if (data.isNotEmpty) {
      return data;
    } else {
      return [];
    }
  }

  @override
  Future<List<LocalPeople?>> getPeopple() async {
    String url = "https://swapi.dev/api/people/";
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    filmDao = databse.filmDao;
    spesiesDao = databse.spesiesDao;

    Dio dio = Dio();
    late int id;
    late int idfilm;
    late int idSpesies;
    var people = await peopleDao.findlastId();
    if (people != null && people.id != null) {
      id = people.id+1;
    } else {
      id = 0;
    }

    final response = await dio.get(url);
    PeopleModel peopleModel = PeopleModel.fromJson(response.data);
    for (int i = 0; i < peopleModel.results.length; i++) {
      peopleDao.insertPeople(LocalPeople(
          id + i,
          peopleModel.results[i].name,
          peopleModel.results[i].height,
          peopleModel.results[i].mass,
          peopleModel.results[i].hairColor,
          peopleModel.results[i].skinColor,
          peopleModel.results[i].eyeColor,
          peopleModel.results[i].birthYear,
          peopleModel.results[i].gender,
          "0"));

      if (peopleModel.results[i].films.isNotEmpty) {
        var filmid = await filmDao.findFilmlastId();
        if (filmid != null && filmid.id != null) {
          idfilm = filmid.id+1;
        } else {
          idfilm = 0;
        }
        for (int j = 0; j < peopleModel.results[i].films.length; j++) {
          filmDao.insertUser(LocalFilm(idfilm + j, id + i,
              "Filem " + j.toString(), peopleModel.results[i].films[j]));
        }
      }
      if (peopleModel.results[i].species.isNotEmpty) {
        var spesiesid = await spesiesDao.findSpecieslastId();
        if (spesiesid != null && spesiesid.id != null) {
          idSpesies = spesiesid.id+1;
        } else {
          idSpesies = 0;
        }
        for (int k = 0; k < peopleModel.results[i].species.length; k++) {
          spesiesDao.insertSpesies(LocalSpecies(idSpesies + k, id + i,
              "Spesies" + k.toString(), peopleModel.results[i].species[k]));
        }
      }
    }
    var data = await peopleDao.findPeoppleall();
    if (data.isNotEmpty) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("data", "ada");
      return data;
    } else {
      return [];
    }
  }

  @override
  Future<List<LocalPeople?>> getLocalAllPeopleSort(String tipe) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    late List<LocalPeople?> list;
    if (tipe == "asc") {
      list = await peopleDao.findPeoppleAsc();
    } else {
      list = await peopleDao.findPeoppleDesc();
    }

    if (list.isNotEmpty) {
      return list;
    } else {
      return [];
    }
  }

  @override
  Future<List<LocalPeople?>> getPeoppleSearch(String kunci, String tipe) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    late List<LocalPeople?> list;
    if (tipe == "asc") {
      list = await peopleDao.findPeopleby("%" + kunci + "%");
    } else {
      list = await peopleDao.findPeopleby("%" + kunci + "%");
    }

    if (list.isNotEmpty) {
      return list;
    } else {
      return [];
    }
  }

  @override
  Future<String> AddPeopple(
      String name,
      String height,
      String mass,
      String hainColor,
      String skincolor,
      String eyeColor,
      String brithyear,
      String gender) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    var getlastId = await peopleDao.findlastId();
    late int id;
    if (getlastId != null && getlastId.id != null) {
      id = getlastId.id + 1;
    } else {
      id = 0;
    }

    LocalPeople localPeople = LocalPeople(id, name, height, mass, hainColor,
        skincolor, eyeColor, brithyear, gender, "0");
    await peopleDao.insertPeople(localPeople);
    var data = await peopleDao.findPeoplebyId(id);
    if (data != null && data.id != null) {
      return "sukses";
    } else {
      return "gagal";
    }
  }

  @override
  Future<LocalPeople?> EditPeopple(
      int id,
      String name,
      String height,
      String mass,
      String hainColor,
      String skincolor,
      String eyeColor,
      String brithyear,
      String gender) async {
    print("data" + name);
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    await peopleDao.uddateDatapeople(id, name, height, mass, hainColor,
        skincolor, eyeColor, brithyear, gender);
    var data = await peopleDao.findPeoplebyId(id);
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }

  @override
  Future<String> HapusPeopple(int id) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    await peopleDao.deleteAllUserbyid(id);
    var data = await peopleDao.findPeoplebyId(id);
    if (data != null && data.id != null) {
      return "gagal";
    } else {
      return "sukses";
    }
  }

  @override
  Future<String> FavoritPeopple(int id, String favorit) async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    late String fav;
    if (favorit == "1") {
      fav = "0";
    } else {
      fav = "1";
    }
    await peopleDao.uddateFavoritpeople(id, fav);
    var data = await peopleDao.findPeoplebyId(id);
    print(data!.favorit);
    if (data != null && data.favorit == fav) {
      return "sukses";
    } else {
      return "gagal";
    }
  }

  @override
  Future<List<LocalPeople?>> getLocalFavoritPeople() async {
    final databse = await $FloorAppDatabase
        .databaseBuilder(DataHelper.databasename)
        .build();
    peopleDao = databse.peoppleDao;
    var data = await peopleDao.findfavoritPeople();
    if (data.isNotEmpty) {
      return data;
    } else {
      return [];
    }
  }
}
