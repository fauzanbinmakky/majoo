import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/local/local_people.dart';

class ItemPeopleGrid extends StatelessWidget {
  LocalPeople? localPeople;
  final GestureTapCallback onTap;
  final GestureTapCallback onTapFavorite;

  ItemPeopleGrid(this.localPeople,
      {required this.onTap, required this.onTapFavorite});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorHelper.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell(
              onTap: onTap,
              child: Text(
                localPeople!.name,
                textAlign: TextAlign.center,
              )),
          InkWell(
            onTap: onTapFavorite,
            child: Icon(
              Icons.favorite,
              color: localPeople!.favorit == "1"
                  ? ColorHelper.primary
                  : ColorHelper.textsecondary,
              size: 24.0,
              semanticLabel: 'Text to announce in accessibility modes',
            ),
          )
        ],
      ),
    );
  }
}
