import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/profil/profil_bloc.dart';
import 'package:test_majoo/bloc/profil/profil_state.dart';

class Profil extends StatefulWidget {
  const Profil({Key? key}) : super(key: key);

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              children: [
                Text("Nama:"),
                SizedBox(
                  width: 10,
                ),
                BlocBuilder<ProfilBloc, ProfilState>(
                  builder: (context, state) {
                    if (state is ProfilSukses) {
                      return Text(state.nama);
                    } else {
                      return Text("Loading");
                    }
                  },
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text("UserId:"),
                SizedBox(
                  width: 10,
                ),
                BlocBuilder<ProfilBloc, ProfilState>(
                  builder: (context, state) {
                    if (state is ProfilSukses) {
                      return Text(state.userID);
                    } else {
                      return Text("Loading");
                    }
                  },
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text("Alamat:"),
                SizedBox(
                  width: 10,
                ),
                BlocBuilder<ProfilBloc, ProfilState>(
                  builder: (context, state) {
                    if (state is ProfilSukses) {
                      return Text(state.alamat);
                    } else {
                      return Text("Loading");
                    }
                  },
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text("Gender:"),
                SizedBox(
                  width: 10,
                ),
                BlocBuilder<ProfilBloc, ProfilState>(
                  builder: (context, state) {
                    if (state is ProfilSukses) {
                      return Text(state.gender);
                    } else {
                      return Text("Loading");
                    }
                  },
                )
              ],
            ),


          ],
        ),
      ),
    );
  }
}
