import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/addpeaople/add_peaople_bloc.dart';
import 'package:test_majoo/bloc/addpeaople/add_people_event.dart';
import 'package:test_majoo/bloc/addpeaople/add_people_state.dart';
import 'package:test_majoo/helper/color_helper.dart';

class AddPeople extends StatefulWidget {
  const AddPeople({Key? key}) : super(key: key);

  @override
  _AddPeopleState createState() => _AddPeopleState();
}

class _AddPeopleState extends State<AddPeople> {
  late String nama;
  late String height;
  late String mass;
  late String haircolor;
  late String skincolor;
  late String eyecolor;
  late String brithyear;
  late String gender;
  TextEditingController namaCrtl = new TextEditingController();
  TextEditingController heightCrtl = new TextEditingController();
  TextEditingController massCrtl = new TextEditingController();
  TextEditingController haircolorCrtl = new TextEditingController();
  TextEditingController skinclorCrtl = new TextEditingController();
  TextEditingController eyecolorCrtl = new TextEditingController();
  TextEditingController brithyearCrtl = new TextEditingController();
  TextEditingController genderCrtl = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.darkprimary,
        title: Text("Tambah Data"),
      ),
      body: BlocListener<AddPeopleBloc, AddPeopleState>(
        listener: (context, state) {
          if (state is AddPeopleSucses) {
            Navigator.pop(context);
          } else if (state is AddPeopleGagal) {
            Scaffold.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text("Gagal Tambah Data")));
          }
        },
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: ListView(
                children: [
                  TextFormField(
                    minLines: 1,
                    controller: namaCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      nama = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'nama',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: heightCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    autocorrect: false,
                    onChanged: (value) {
                      height = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Height',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: massCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    autocorrect: false,
                    onChanged: (value) {
                      mass = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Mass',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: haircolorCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      haircolor = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Hair Color',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: skinclorCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      skincolor = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Skin Color',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: eyecolorCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      eyecolor = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Eye Color',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: brithyearCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      brithyear = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Brith year',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    minLines: 1,
                    controller: genderCrtl,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    onChanged: (value) {
                      gender = value;
                    },
                    onEditingComplete: () {},
                    decoration: InputDecoration(
                      hintText: 'Gender',
                      filled: true,
                      fillColor: ColorHelper.bggrey,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: ColorHelper.linegrey),
                      ),
                      contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 60),
                      primary: ColorHelper.primary,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      padding: EdgeInsets.symmetric(vertical: 2.0)),
                  child: Text(
                    "Tambah",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    context.read<AddPeopleBloc>().add(DoaddPeople(
                        namaCrtl.text,
                        heightCrtl.text,
                        massCrtl.text,
                        haircolorCrtl.text,
                        skinclorCrtl.text,
                        eyecolorCrtl.text,
                        brithyearCrtl.text,
                        genderCrtl.text));
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
