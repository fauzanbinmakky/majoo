import 'package:flutter/material.dart';
import 'package:test_majoo/bloc/detail_people/film_bloc.dart';
import 'package:test_majoo/bloc/detail_people/film_event.dart';
import 'package:test_majoo/bloc/detail_people/film_state.dart';
import 'package:test_majoo/bloc/login/login_event.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_bloc.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_event.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_state.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/local/local_people.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/ui/item_film.dart';
import 'dialog_peringantan.dart';
import 'edit_people.dart';

class DetailProfil extends StatefulWidget {
  LocalPeople? localPeople;

  DetailProfil(this.localPeople);

  @override
  _DetailProfilState createState() => _DetailProfilState();
}

class _DetailProfilState extends State<DetailProfil> {
  @override
  void initState() {
    context.read<UbahPeopleBloc>().add(Toinitial());
    context.read<FilmBloc>().add(DoGetFil(widget.localPeople!.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.darkprimary,
        title: Text("Detail Data"),
      ),
      body: BlocListener<UbahPeopleBloc, UbahPeopleState>(
        listener: (context, state) {
          if (state is HapusDataSucses) {
            Navigator.pop(context, "hapus");
          } else if (state is HapusDataGagal) {
            Scaffold.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(content: Text("Hapus Data Gagal")),
              );
          } else if (state is RefreshData) {
            widget.localPeople = state.localPeople;
          }
        },
        child: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                children: [
                  Text("Nama:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.name);
                      } else {
                        return Text(widget.localPeople!.name);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Height:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.height);
                      } else {
                        return Text(widget.localPeople!.height);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Mass:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.mass);
                      } else {
                        return Text(widget.localPeople!.mass);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Hair Color:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.hair_color);
                      } else {
                        return Text(widget.localPeople!.hair_color);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Skin Color:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.skin_color);
                      } else {
                        return Text(widget.localPeople!.skin_color);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Eye Color:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.eye_color);
                      } else {
                        return Text(widget.localPeople!.eye_color);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Brith Year:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.birth_year);
                      } else {
                        return Text(widget.localPeople!.birth_year);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text("Gender:"),
                  SizedBox(
                    width: 10,
                  ),
                  BlocBuilder<UbahPeopleBloc, UbahPeopleState>(
                    builder: (context, state) {
                      if (state is RefreshData) {
                        return Text(state.localPeople!.gender);
                      } else {
                        return Text(widget.localPeople!.gender);
                      }
                    },
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
             
              Container(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery
                          .of(context)
                          .size
                          .width, 60),
                      primary: ColorHelper.primary,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      padding: EdgeInsets.symmetric(vertical: 2.0)),
                  child: Text(
                    "Edit",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, DataHelper.toEdit,
                        arguments: EditPeople(widget.localPeople))
                        .then((value) => refreshData(value));
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery
                          .of(context)
                          .size
                          .width, 60),
                      primary: ColorHelper.primary,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      padding: EdgeInsets.symmetric(vertical: 2.0)),
                  child: Text(
                    "Hapus",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext contex) =>
                            DialogPeringatan(
                                isi: "hapus",
                                onOK: () {
                                  Navigator.pop(context);
                                  context
                                      .read<UbahPeopleBloc>()
                                      .add(DoHapusData(widget.localPeople!.id));
                                }));
                  },
                ),
              ),

              Expanded(
                child: BlocBuilder<FilmBloc, FilmState>(builder: (context, state) {
                  if (state is FilmAda) {
                    return ListView.builder(
                      itemCount: state.list.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ItemFilm(state.list[index], onTap: () {});
                        });
                  } else {
                    return Container();
                  }
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  refreshData(Object? value) {
    if (value != null) {
      value as LocalPeople;
      context.read<UbahPeopleBloc>().add(DoRefreshData(value));
    }
  }
}
