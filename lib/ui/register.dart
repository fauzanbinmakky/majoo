import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/login/show_password_bloc.dart';
import 'package:test_majoo/bloc/login/show_password_event.dart';
import 'package:test_majoo/bloc/login/show_paswwrd_state.dart';
import 'package:test_majoo/bloc/register/register_bloc.dart';
import 'package:test_majoo/bloc/register/register_event.dart';
import 'package:test_majoo/bloc/register/register_state.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/helper/data_helper.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  late String userID;
  TextEditingController useridCrtl = TextEditingController();
  late String nama;
  TextEditingController namaCrtl = TextEditingController();
  late String alamat;
  TextEditingController alamatCrtl = TextEditingController();
  late String password;
  TextEditingController passwordCrtl = TextEditingController();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<RegisterBloc, RegisterState>(listener: (contex, state) {
          if (state is RegisterSukses) {
            Navigator.pushNamedAndRemoveUntil(
                context, DataHelper.toLogin, (route) => false);
          }
        }),
        BlocListener<ShowPasswordBloc, ShowPasswordState>(
            listener: (context, state) {
          if (state is PasswordVisibilitDiganti) {
            hidePassword = state.visibilty;
          }
        })
      ],
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorHelper.darkprimary,
          title: Text("Daftar Applikasi"),
        ),
        body: Container(
          padding: EdgeInsets.all(15),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  "Daftar",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                minLines: 1,
                controller: useridCrtl,
                maxLines: 1,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value) {
                  userID = value;
                },
                onEditingComplete: () {},
                decoration: InputDecoration(
                  hintText: 'UserId',
                  filled: true,
                  fillColor: ColorHelper.bggrey,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                minLines: 1,
                controller: namaCrtl,
                maxLines: 1,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value) {
                  nama = value;
                },
                onEditingComplete: () {},
                decoration: InputDecoration(
                  hintText: 'Nama',
                  filled: true,
                  fillColor: ColorHelper.bggrey,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                minLines: 1,
                controller: alamatCrtl,
                maxLines: 1,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value) {
                  alamat = value;
                },
                onEditingComplete: () {},
                decoration: InputDecoration(
                  hintText: 'Alamat',
                  filled: true,
                  fillColor: ColorHelper.bggrey,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<ShowPasswordBloc, ShowPasswordState>(
                builder: (context, state) {
                  return TextFormField(
                    minLines: 1,
                    maxLines: 1,
                    obscureText: hidePassword,
                    controller: passwordCrtl,
                    onChanged: (value) {
                      password = value;
                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    autocorrect: false,
                    decoration: InputDecoration(
                        hintText: 'Kata Sandi',
                        filled: true,
                        fillColor: ColorHelper.bggrey,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: ColorHelper.linegrey),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: ColorHelper.linegrey),
                        ),
                        suffixIcon: IconButton(
                            icon: Icon(!hidePassword
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              context
                                  .read<ShowPasswordBloc>()
                                  .add(DoGantiVisbilityPassword(hidePassword));
                            }),
                        contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2)),
                  );
                },
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 60),
                      primary: ColorHelper.primary,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      padding: EdgeInsets.symmetric(vertical: 2.0)),
                  child: BlocBuilder<RegisterBloc, RegisterState>(
                    builder: (context, state) {
                      if (state is RegisterLoading) {
                        return Text(
                          "Loading . . .",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        );
                      } else {
                        return Text(
                          "Daftar",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        );
                      }
                    },
                  ),
                  onPressed: () {
                    context
                        .read<RegisterBloc>()
                        .add(doRegister(userID, nama, password, alamat, "L"));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
