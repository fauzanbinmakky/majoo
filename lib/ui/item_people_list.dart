import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/local/local_people.dart';

class ItemPeopleList extends StatelessWidget {
  LocalPeople? localPeople;
  final GestureTapCallback onTap;
  final GestureTapCallback onTapFavorite;

  ItemPeopleList(this.localPeople,
      {required this.onTap, required this.onTapFavorite});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: onTap,
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.64,
                      child: Text(
                        localPeople!.name,
                      )),
                ),
                InkWell(
                  onTap: onTapFavorite,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.15,
                    child: Icon(
                      Icons.favorite,
                      color: localPeople!.favorit == "1"
                          ? ColorHelper.primary
                          : ColorHelper.textsecondary,
                      size: 24.0,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
