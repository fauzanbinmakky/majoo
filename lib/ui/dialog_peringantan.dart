import 'package:flutter/material.dart';

class DialogPeringatan extends StatefulWidget {
  final GestureTapCallback onOK;
  String isi;

  DialogPeringatan({required this.isi, required this.onOK});

  @override
  _DialogPeringatanState createState() => _DialogPeringatanState();
}

class _DialogPeringatanState extends State<DialogPeringatan> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: contentBox(context),
    );
  }

  contentBox(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text(
            "Peringantan",
            style: TextStyle(fontSize: 17),
          ),
          SizedBox(
            height: 15,
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              widget.isi == "edit"
                  ? "Apakah Anda Yakin untuk Mengubah Data ini?"
                  : "Apakah Anda Yakin untuk Menghapus Data ini?",
              style: TextStyle(fontSize: 17),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text("Batal",
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              ),
              SizedBox(
                width: 20,
              ),
              InkWell(
                onTap: widget.onOK,
                child: Text(widget.isi == "edit" ? "Edit" : "Hapus",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              SizedBox(
                width: 16,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
