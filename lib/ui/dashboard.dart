import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/bloc/beranda/navbar_bloc.dart';
import 'package:test_majoo/bloc/beranda/navbar_event.dart';
import 'package:test_majoo/bloc/beranda/navbar_state.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/ui/beranda.dart';
import 'package:test_majoo/ui/favorit.dart';
import 'package:test_majoo/ui/profil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with WidgetsBindingObserver {
  int selectedTabIndex = 0;
  final List<Widget> _children = [
    Beranda(),
    Favorit(),
    Profil(),
  ];
  final List<String> _judul = [
    "Beranda",
    "Favorit",
    "Profil",
  ];

  @override
  Widget build(BuildContext context) {
    return BlocListener<NavBarBloc, NavBarState>(
      listener: (context, state) {
        if (state is PindahNavbar) {
          selectedTabIndex = state.posisi;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          title: Row(
            children: [
              BlocBuilder<NavBarBloc, NavBarState>(
                builder: (context, state) {
                  return Text(
                    _judul[selectedTabIndex],
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  );
                },
              )
            ],
          ),
          backgroundColor: ColorHelper.darkprimary,
        ),
        body: BlocBuilder<NavBarBloc, NavBarState>(
          builder: (context, state) {
            return _children[selectedTabIndex];
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: ColorHelper.white,
          onTap: onTabTapped,
          selectedItemColor: ColorHelper.primary,
          unselectedItemColor: ColorHelper.textsecondary,
          currentIndex: selectedTabIndex,
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: BlocBuilder<NavBarBloc, NavBarState>(
                  builder: (context, state) {
                return Icon(
                  Icons.home,
                  color: selectedTabIndex == 0
                      ? ColorHelper.primary
                      : ColorHelper.textsecondary,
                  size: 24.0,
                  semanticLabel: 'Text to announce in accessibility modes',
                );
              }),
              label: "Beranda",
            ),
            BottomNavigationBarItem(
              icon: BlocBuilder<NavBarBloc, NavBarState>(
                builder: (context, state) {
                  return Icon(
                    Icons.favorite,
                    color: selectedTabIndex == 1
                        ? ColorHelper.primary
                        : ColorHelper.textsecondary,
                    size: 24.0,
                    semanticLabel: 'Text to announce in accessibility modes',
                  );
                },
              ),
              label: "Favorit",
            ),
            BottomNavigationBarItem(
                icon: BlocBuilder<NavBarBloc, NavBarState>(
                  builder: (context, state) {
                    return Icon(
                      Icons.people,
                      color: selectedTabIndex == 2
                          ? ColorHelper.primary
                          : ColorHelper.textsecondary,
                      size: 24.0,
                      semanticLabel: 'Text to announce in accessibility modes',
                    );
                  },
                ),
                label: "Profile")
          ],
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    context.read<NavBarBloc>().add(GantiTab(index));
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance!.removeObserver(this);
  }
}
