import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/favorit_bloc.dart';
import 'package:test_majoo/bloc/beranda/favorit_state.dart';
import 'package:test_majoo/bloc/beranda/favorite_event.dart';
import 'package:test_majoo/bloc/beranda/people_bloc.dart';
import 'package:test_majoo/bloc/beranda/people_event.dart';
import 'package:test_majoo/bloc/beranda/people_state.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/ui/item_people_list.dart';

import 'detail_profile.dart';

class Favorit extends StatefulWidget {
  const Favorit({Key? key}) : super(key: key);

  @override
  _FavoritState createState() => _FavoritState();
}

class _FavoritState extends State<Favorit> {
  @override
  void initState() {
    context.read<PeopleBloc>().add(getDataFavorit());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<FavoritBloc,FavoritState>(
        listener: (context,state){
          if(state is FavoritBerhasilDiganti){
            context.read<PeopleBloc>().add(getDataFavorit());
          }
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: BlocBuilder<PeopleBloc, PeoppleState>(
            builder: (context, state) {
              if (state is PeoppleSucsesList) {
                return ListView.builder(
                    itemCount: state.list.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ItemPeopleList(state.list[index], onTap: () {
                        Navigator.pushNamed(context, DataHelper.toDetail,
                                arguments: DetailProfil(state.list[index]))
                            .whenComplete(() => context
                                .read<PeopleBloc>()
                                .add(getDataFavorit()));
                      }, onTapFavorite: () {
                        context.read<FavoritBloc>().add(DoGantiFavorit(
                            state.list[index]!.id, state.list[index]!.favorit));
                      });
                    });
              } else {
                return Container(
                  child: Text("data kososng"),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
