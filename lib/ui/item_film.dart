import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/local/local_film.dart';

class ItemFilm extends StatelessWidget {
  LocalFilm? localFilm;
  final GestureTapCallback onTap;

  ItemFilm(this.localFilm, {required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: onTap, child: Padding(padding:EdgeInsets.all(10),child: Text(localFilm!.judul)));
  }
}
