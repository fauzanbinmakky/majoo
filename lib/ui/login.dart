import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/login/login_bloc.dart';
import 'package:test_majoo/bloc/login/login_event.dart';
import 'package:test_majoo/bloc/login/login_state.dart';
import 'package:test_majoo/bloc/login/show_password_bloc.dart';
import 'package:test_majoo/bloc/login/show_password_event.dart';
import 'package:test_majoo/bloc/login/show_paswwrd_state.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:test_majoo/helper/data_helper.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late String userID;
  TextEditingController useridCrtl = new TextEditingController();
  late String nama;
  TextEditingController namaCrtl = new TextEditingController();
  late String alamat;
  TextEditingController alamatCrtl = new TextEditingController();
  late String password;
  TextEditingController passwordCrtl = new TextEditingController();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.darkprimary,
        title: Text("Masuk Applikasi"),
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener<LoginBloc, LoginState>(listener: (context, state) {
            if (state is LoginSukses) {
              Navigator.pushNamedAndRemoveUntil(
                  context, DataHelper.toHome, (route) => false);
            } else if (state is LoginGagal) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text("Login Gagal")),
                );
            } else if (state is TidakAdaInternet) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text("Tidak Ada Koneksi Internet")),
                );
            }
          }),
          BlocListener<ShowPasswordBloc, ShowPasswordState>(
              listener: (context, state) {
            if (state is PasswordVisibilitDiganti) {
              hidePassword = state.visibilty;
            }
          })
        ],
        child: Container(
          padding: EdgeInsets.all(15),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  "Login",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                minLines: 1,
                controller: useridCrtl,
                maxLines: 1,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value) {
                  userID = value;
                },
                onEditingComplete: () {},
                decoration: InputDecoration(
                  hintText: 'UserId',
                  filled: true,
                  fillColor: ColorHelper.bggrey,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: ColorHelper.linegrey),
                  ),
                  contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<ShowPasswordBloc, ShowPasswordState>(
                builder: (context, state) {
                  return TextFormField(
                    minLines: 1,
                    maxLines: 1,
                    obscureText: hidePassword,
                    controller: passwordCrtl,
                    onChanged: (value) {
                      password = value;
                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    autocorrect: false,
                    decoration: InputDecoration(
                        hintText: 'Kata Sandi',
                        filled: true,
                        fillColor: ColorHelper.bggrey,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: ColorHelper.linegrey),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: ColorHelper.linegrey),
                        ),
                        suffixIcon: IconButton(
                            icon: Icon(!hidePassword
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              context
                                  .read<ShowPasswordBloc>()
                                  .add(DoGantiVisbilityPassword(hidePassword));
                            }),
                        contentPadding: EdgeInsets.fromLTRB(15, 2, 15, 2)),
                  );
                },
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, DataHelper.toRegister);
                  },
                  child: Center(child: Text("Register"))),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(MediaQuery.of(context).size.width, 60),
                      primary: ColorHelper.primary,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      padding: EdgeInsets.symmetric(vertical: 2.0)),
                  child: BlocBuilder<LoginBloc, LoginState>(
                    builder: (context, state) {
                      if (state is LoginLoading) {
                        return Text(
                          "Loading . . .",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        );
                      } else {
                        return Text(
                          "Masuk",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        );
                      }
                    },
                  ),
                  onPressed: () {
                    context.read<LoginBloc>().add(doLogin(userID, password));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
