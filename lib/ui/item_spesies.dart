import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/local/local_spesies.dart';

class ItemSpesies extends StatelessWidget {
  LocalSpecies? localSpecies;
  final GestureTapCallback onTap;

  ItemSpesies(this.localSpecies, {required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: onTap, child: Text(localSpecies!.judul));
  }
}
