import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/bloc/beranda/favorit_bloc.dart';
import 'package:test_majoo/bloc/beranda/favorit_state.dart';
import 'package:test_majoo/bloc/beranda/favorite_event.dart';
import 'package:test_majoo/bloc/beranda/people_bloc.dart';
import 'package:test_majoo/bloc/beranda/people_event.dart';
import 'package:test_majoo/bloc/beranda/people_state.dart';
import 'package:test_majoo/bloc/beranda/search_bloc.dart';
import 'package:test_majoo/bloc/beranda/search_event.dart';
import 'package:test_majoo/bloc/beranda/search_state.dart';
import 'package:test_majoo/bloc/beranda/sort_bloc.dart';
import 'package:test_majoo/bloc/beranda/sort_event.dart';
import 'package:test_majoo/bloc/beranda/sort_state.dart';
import 'package:test_majoo/bloc/beranda/view_bloc.dart';
import 'package:test_majoo/bloc/beranda/view_event.dart';
import 'package:test_majoo/bloc/beranda/view_state.dart';
import 'package:test_majoo/helper/color_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/ui/detail_profile.dart';
import 'package:test_majoo/ui/item_people_grid.dart';
import 'package:test_majoo/ui/item_people_list.dart';

class Beranda extends StatefulWidget {
  const Beranda({Key? key}) : super(key: key);

  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  int selectedsort = 0;
  int selectedview = 0;
  String kunci = "";
  late String userID;
  TextEditingController useridCrtl = new TextEditingController();

  @override
  void initState() {
    context
        .read<PeopleBloc>()
        .add(getDataPeople(selectedview, selectedsort, kunci));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocListener(
        listeners: [
          BlocListener<ViewBloc, ViewState>(listener: (context, state) {
            if (state is ViewBerubah) {
              selectedview = state.posisi;
              context
                  .read<PeopleBloc>()
                  .add(getDataPeople(selectedview, selectedsort, kunci));
            }
          }),
          BlocListener<SortBloc, SortState>(listener: (context, state) {
            if (state is UrutanBerubah) {
              selectedsort = state.posisi;
              context
                  .read<PeopleBloc>()
                  .add(getDataPeople(selectedview, selectedsort, kunci));
            }
          }),
          BlocListener<FavoritBloc, FavoritState>(listener: (context, state) {
            if (state is FavoritBerhasilDiganti) {
              context
                  .read<PeopleBloc>()
                  .add(getDataPeople(selectedview, selectedsort, kunci));
            }
          }),
          BlocListener<SearchBloc, SearchState>(listener: (context, state) {
            if (state is SearchBerubah) {
              kunci = state.kunci;
            }
          })
        ],
        child: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      context.read<SortBloc>().add(GantiUrutan(0));
                    },
                    child: BlocBuilder<SortBloc,SortState>(
                      builder: (context,state){
                        return Icon(
                          Icons.arrow_circle_up_outlined,
                          color: selectedsort == 0
                              ? ColorHelper.primary
                              : ColorHelper.textsecondary,
                          size: 30.0,
                          semanticLabel: 'Text to announce in accessibility modes',
                        );
                      },

                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {
                      context.read<SortBloc>().add(GantiUrutan(1));
                    },
                    child: BlocBuilder<SortBloc, SortState>(
                      builder: (context, state) {
                        return Icon(
                          Icons.arrow_circle_down_outlined,
                          color: selectedsort == 1
                              ? ColorHelper.primary
                              : ColorHelper.textsecondary,
                          size: 30.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {
                      context.read<ViewBloc>().add(GantiModelView(0));
                    },
                    child: BlocBuilder<ViewBloc, ViewState>(
                      builder: (context, state) {
                        return Icon(
                          Icons.list,
                          color: selectedview == 0
                              ? ColorHelper.primary
                              : ColorHelper.textsecondary,
                          size: 30.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {
                      context.read<ViewBloc>().add(GantiModelView(1));
                    },
                    child: BlocBuilder<ViewBloc, ViewState>(
                      builder: (context, state) {
                        return Icon(
                          Icons.grid_3x3_outlined,
                          color: selectedview == 1
                              ? ColorHelper.primary
                              : ColorHelper.textsecondary,
                          size: 30.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        );
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              CupertinoSearchTextField(
                onChanged: (v) {
                  context.read<SearchBloc>().add(GantiKunciSearch(v));
                  context
                      .read<PeopleBloc>()
                      .add(getDataPeople(selectedview, selectedsort, v));
                },
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: BlocBuilder<PeopleBloc, PeoppleState>(
                    builder: (context, state) {
                  if (state is PeoppleSucsesList) {
                    return ListView.builder(
                        itemCount: state.list.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ItemPeopleList(
                            state.list[index],
                            onTap: () {
                              Navigator.pushNamed(context, DataHelper.toDetail,
                                      arguments:
                                          DetailProfil(state.list[index]))
                                  .whenComplete(() => context
                                      .read<PeopleBloc>()
                                      .add(getDataPeople(
                                          selectedview, selectedsort, kunci)));
                            },
                            onTapFavorite: () {
                              context.read<FavoritBloc>().add(DoGantiFavorit(
                                  state.list[index]!.id,
                                  state.list[index]!.favorit));
                            },
                          );
                        });
                  } else if (state is PeoppleSucsesGrid) {
                    return GridView.builder(
                        itemCount: state.list.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 1,
                            crossAxisSpacing: 1,
                            crossAxisCount: 3,
                            mainAxisSpacing: 1),
                        itemBuilder: (BuildContext context, int index) {
                          return ItemPeopleGrid(
                            state.list[index],
                            onTap: () {
                              Navigator.pushNamed(context, DataHelper.toDetail,
                                      arguments:
                                          DetailProfil(state.list[index]))
                                  .whenComplete(() => context
                                      .read<PeopleBloc>()
                                      .add(getDataPeople(
                                          selectedview, selectedsort, kunci)));
                            },
                            onTapFavorite: () {
                              context.read<FavoritBloc>().add(DoGantiFavorit(
                                  state.list[index]!.id,
                                  state.list[index]!.favorit));
                            },
                          );
                        });
                  } else if (state is PeopleKosong) {
                    return Container(
                      child: Center(child: Text("kosong")),
                    );
                  } else {
                    return Container(
                      child: Center(child: Text("loading")),
                    );
                  }
                }),
              ),
              FloatingActionButton.small(
                backgroundColor: ColorHelper.darkprimary,
                onPressed: () {
                  Navigator.pushNamed(context, DataHelper.toAdd).whenComplete(
                      () => context.read<PeopleBloc>().add(
                          getDataPeople(selectedview, selectedsort, kunci)));
                },
                child: Icon(
                  Icons.add,
                  color: ColorHelper.white,
                  size: 24.0,
                  semanticLabel: 'Text to announce in accessibility modes',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
