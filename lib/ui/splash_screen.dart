import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/auth/auth_bloc.dart';
import 'package:test_majoo/bloc/auth/auth_state.dart';
import 'package:test_majoo/helper/data_helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Authenticated) {
            Navigator.pushNamedAndRemoveUntil(
                context, DataHelper.toHome, (route) => false);
          } else {
            Navigator.pushNamedAndRemoveUntil(
                context, DataHelper.toLogin, (route) => false);
          }
        },
        child: Container(
          child: Center(
            child: Text("SplahsScren"),
          ),
        ),
      ),
    );
  }
}
