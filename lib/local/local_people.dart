import 'package:floor/floor.dart';

@entity
class LocalPeople {
  @PrimaryKey(autoGenerate: true)
  final int id;
  String name;
  String height;
  String mass;
  String hair_color;
  String skin_color;
  String eye_color;
  String birth_year;
  String gender;
  String favorit;

  LocalPeople(
      this.id,
      this.name,
      this.height,
      this.mass,
      this.hair_color,
      this.skin_color,
      this.eye_color,
      this.birth_year,
      this.gender,
      this.favorit);
}
