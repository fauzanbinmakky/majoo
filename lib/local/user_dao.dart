import 'package:floor/floor.dart';
import 'package:test_majoo/local/local_user.dart';

@dao
abstract class UserDao {
  @insert
  Future<void> insertUser(LocalUser user);

  @Query('SELECT * FROM LocalUser WHERE token =:token')
  Future<LocalUser?> findUseron(String token);

  @Query('SELECT * FROM LocalUser WHERE userid =:userid')
  Future<LocalUser?> cekUserId(String userid);

  @Query('SELECT * FROM LocalUser WHERE userid =:userid and password=:password')
  Future<LocalUser?> cekLogin(String userid, String password);

  @Query('DELETE FROM LocalUser')
  Future<void> deleteAllUser();

  @Query('DELETE FROM LocalUser where token =:token')
  Future<void> deleteAllUserbyToken(String token);
}
