import 'package:floor/floor.dart';
import 'package:test_majoo/local/local_spesies.dart';

@dao
abstract class SpesiesDao {
  @insert
  Future<void> insertSpesies(LocalSpecies species);

  @Query('SELECT * FROM LocalSpecies WHERE idPeople =:id')
  Future<List<LocalSpecies?>> findSpesiesbyId(int id);

  @Query('SELECT * FROM LocalSpecies order by id desc limit 1')
  Future<LocalSpecies?> findSpecieslastId();
}
