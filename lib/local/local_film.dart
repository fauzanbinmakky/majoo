import 'package:floor/floor.dart';

@entity
class LocalFilm {
  @PrimaryKey(autoGenerate: true)
  int id;
  int idPeople;
  String judul;
  String film;

  LocalFilm(this.id, this.idPeople, this.judul, this.film);
}
