import 'package:floor/floor.dart';
import 'package:test_majoo/local/local_people.dart';

@dao
abstract class PeopleDao {
  @insert
  Future<void> insertPeople(LocalPeople people);

  @Query('SELECT * FROM LocalPeople order by id asc')
  Future<List<LocalPeople?>> findPeoppleAsc();

  @Query('SELECT * FROM LocalPeople WHERE id =:id limit 1')
  Future<LocalPeople?> findPeoplebyId(int id);

  @Query('SELECT * FROM LocalPeople order by id desc')
  Future<List<LocalPeople?>> findPeoppleDesc();

  @Query('SELECT * FROM LocalPeople')
  Future<List<LocalPeople?>> findPeoppleall();

  @Query('SELECT * FROM LocalPeople order by id desc limit 1')
  Future<LocalPeople?> findlastId();

  @update
  Future<void> updatePeople(LocalPeople people);

  @Query(
      'SELECT * FROM LocalPeople where name LIKE :kunci  or (height LIKE :kunci) or (mass LIKE :kunci) or (hair_color LIKE :kunci) or (skin_color LIKE :kunci) or (eye_color LIKE :kunci) or (birth_year LIKE :kunci) or (gender LIKE :kunci) order by id asc')
  Future<List<LocalPeople?>> findPeopleby(String kunci);

  @Query('SELECT * FROM LocalPeople WHERE favorit ="1"')
  Future<List<LocalPeople?>> findfavoritPeople();

  @Query('DELETE FROM LocalPeople')
  Future<void> deleteAllPeople();

  @Query('DELETE FROM LocalPeople where id =:id')
  Future<void> deleteAllUserbyid(int id);

  @Query(
      'Update LocalPeople SET name= :name ,height= :height, mass=:mass,hair_color =:hair_color,skin_color =:skin_color,eye_color =:eye_color, birth_year =:birth_year, gender=:gender WHERE id =:id')
  Future<void> uddateDatapeople(
      int id,
      String name,
      String height,
      String mass,
      String hair_color,
      String skin_color,
      String eye_color,
      String birth_year,
      String gender);

  @Query('Update LocalPeople SET favorit =:favorit  WHERE id =:id')
  Future<void> uddateFavoritpeople(int id, String favorit);
}
