import 'package:floor/floor.dart';
import 'package:test_majoo/local/local_film.dart';

@dao
abstract class FilmDao {
  @insert
  Future<void> insertUser(LocalFilm film);

  @Query('SELECT * FROM LocalFilm WHERE idPeople =:id')
  Future<List<LocalFilm?>> findfilmbyId(int id);

  @Query('SELECT * FROM LocalFilm order by id desc limit 1')
  Future<LocalFilm?> findFilmlastId();
}
