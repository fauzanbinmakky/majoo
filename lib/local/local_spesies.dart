import 'package:floor/floor.dart';

@entity
class LocalSpecies {
  @PrimaryKey(autoGenerate: true)
  late int id;
  int idPeople;
  String judul;
  String species;

  LocalSpecies(this.id, this.idPeople, this.judul, this.species);
}
