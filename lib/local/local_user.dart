import 'package:floor/floor.dart';

@entity
class LocalUser {
  @primaryKey
  String userid;
  String nama;
  String password;
  String alamat;
  String gender;
  String token;

  LocalUser(this.userid, this.nama, this.password, this.alamat, this.gender,
      this.token);
}
