import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginSukses extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginGagal extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginLoading extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginLoadPeopleSucses extends LoginState {
  @override
  List<Object?> get props => [];
}

class TidakAdaInternet extends LoginState {
  @override
  List<Object?> get props => [];
}
