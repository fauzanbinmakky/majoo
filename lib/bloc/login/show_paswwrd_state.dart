import 'package:equatable/equatable.dart';

abstract class ShowPasswordState extends Equatable {
  const ShowPasswordState();
}

class PasswordVisibilitDiganti extends ShowPasswordState {
  bool visibilty;
  @override
  List<Object?> get props => [visibilty];

  PasswordVisibilitDiganti(this.visibilty);
}



class Passwordinitial extends ShowPasswordState {
  @override
  List<Object?> get props => [];
}
