import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class doLogin extends LoginEvent {
  String userId;
  String password;

  @override
  List<Object?> get props => [userId,password];

  doLogin(this.userId, this.password);
}

class loadPeople extends LoginEvent {
  @override
  List<Object?> get props => [];
}
