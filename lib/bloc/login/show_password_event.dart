import 'package:equatable/equatable.dart';

abstract class ShowPasswordEvent extends  Equatable{


  const ShowPasswordEvent();
}

class DoGantiVisbilityPassword extends ShowPasswordEvent{
  bool isShowing;
  @override

  List<Object?> get props => [isShowing];

  DoGantiVisbilityPassword(this.isShowing);
}