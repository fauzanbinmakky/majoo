import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/login/login_event.dart';
import 'package:test_majoo/bloc/login/login_state.dart';
import 'package:test_majoo/helper/cek_internet.dart';
import 'package:test_majoo/helper/data_helper.dart';
import 'package:test_majoo/repositiry/people_repo.dart';
import 'package:test_majoo/repositiry/user_repo.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  UserRepoImp userRepoImp;
  PeopleRepoImp pepopleRepoImp;

  LoginBloc({required this.userRepoImp, required this.pepopleRepoImp})
      : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is doLogin) {
      yield* _dologin(event);
    } else if (event is loadPeople) {
      yield* _loadPeople(event);
    }
  }

  Stream<LoginState> _dologin(doLogin event) async* {
    yield LoginInitial();
    yield LoginLoading();
    if (await cekInternet()) {
      var data = await userRepoImp.login(event.userId, event.password);

      if (data != "") {
        add(loadPeople());
      } else {
        yield LoginGagal();
      }
    } else {
      yield TidakAdaInternet();
    }
  }

  Stream<LoginState> _loadPeople(loadPeople event) async* {
    yield LoginLoading();
    var data = await pepopleRepoImp.getPeopple();
    if (data.isNotEmpty) {
      yield LoginSukses();
    } else {
      yield LoginGagal();
    }
  }
}
