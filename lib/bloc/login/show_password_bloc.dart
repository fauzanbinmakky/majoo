import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/login/show_password_event.dart';
import 'package:test_majoo/bloc/login/show_paswwrd_state.dart';

class ShowPasswordBloc extends Bloc<ShowPasswordEvent, ShowPasswordState> {
  ShowPasswordBloc() : super(Passwordinitial());

  @override
  Stream<ShowPasswordState> mapEventToState(ShowPasswordEvent event) async* {
    if (event is DoGantiVisbilityPassword) {
      yield* _doGAntiVisibiltyPassword(event);
    }
  }

  Stream<ShowPasswordState> _doGAntiVisibiltyPassword(
      DoGantiVisbilityPassword event) async* {
    yield PasswordVisibilitDiganti(!event.isShowing);
  }
}
