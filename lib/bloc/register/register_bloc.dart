import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/register/register_event.dart';
import 'package:test_majoo/bloc/register/register_state.dart';
import 'package:test_majoo/repositiry/user_repo.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  UserRepoImp registerRepoImp;

  RegisterBloc({required this.registerRepoImp}) : super(RegisterInitial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is doRegister) {
      yield* _doRegister(event);
    }
  }

  Stream<RegisterState> _doRegister(doRegister event) async* {
    yield RegisterLoading();
    var cekUser = await registerRepoImp.cekuser(event.userId);
    if (cekUser) {
      var register = await registerRepoImp.register(
          event.userId,
          event.nama,
          event.password,
          event.alamat,
          event.gender,
          (event.userId + event.password));
      if (register == "1") {
        yield RegisterSukses();
      } else {
        yield RegisterGagal();
      }
    } else {
      yield RegisterGagal();
    }
  }
}
