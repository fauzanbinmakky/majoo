import 'package:equatable/equatable.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterGagal extends RegisterState {
  RegisterGagal();

  @override
  List<Object?> get props => [];
}

class RegisterSukses extends RegisterState {
  RegisterSukses();

  @override
  List<Object?> get props => [];
}

class RegisterInitial extends RegisterState {


  @override
  List<Object?> get props => [];

  RegisterInitial();
}

class RegisterLoading extends RegisterState {


  @override
  List<Object?> get props => [];

  RegisterLoading();
}


