import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}



class doRegister extends RegisterEvent {
  String userId;
  String nama;
  String password;
  String alamat;
  String gender;
  @override
  List<Object?> get props => [userId, nama, password, alamat, gender];

  doRegister(this.userId, this.nama, this.password, this.alamat, this.gender);
}
