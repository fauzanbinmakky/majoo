import 'package:equatable/equatable.dart';

abstract class ProfilState extends Equatable {
  const ProfilState();
}

class ProfilInitial extends ProfilState {
  @override
  List<Object?> get props => [];
}

class ProfilGagal extends ProfilState {
  @override
  List<Object?> get props => [];
}

class ProfilSukses extends ProfilState {
  String nama;
  String userID;
  String alamat;
  String gender;

  @override
  List<Object?> get props => [nama, userID, alamat, gender];

  ProfilSukses(this.nama, this.userID, this.alamat, this.gender);
}
