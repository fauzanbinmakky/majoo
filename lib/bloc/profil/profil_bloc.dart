import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/bloc/profil/profil_event.dart';
import 'package:test_majoo/bloc/profil/profil_state.dart';
import 'package:test_majoo/repositiry/user_repo.dart';

class ProfilBloc extends Bloc<ProfilEvent, ProfilState> {
  UserRepoImp userRepoImp;

  ProfilBloc({required this.userRepoImp}) : super(ProfilInitial());

  @override
  Stream<ProfilState> mapEventToState(ProfilEvent event) async* {
    if (event is getProfileOn) {
      yield* getProfile(event);
    }
  }

  Stream<ProfilState> getProfile(getProfileOn event) async* {
    yield ProfilInitial();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = await userRepoImp.getUserOn(prefs.getString("token")!);
    if (data != null) {
      yield ProfilSukses(data.nama, data.userid, data.alamat, data.gender);
    } else {
      yield ProfilGagal();
    }
  }
}
