import 'package:equatable/equatable.dart';

abstract class AddPeopleState extends Equatable {
  const AddPeopleState();
}

class AddPeopleSucses extends AddPeopleState {
  @override
  List<Object?> get props => [];
}

class AddPeopleGagal extends AddPeopleState {
  @override
  List<Object?> get props => [];
}

class AddPeopleInitial extends AddPeopleState {
  @override
  List<Object?> get props => [];
}
