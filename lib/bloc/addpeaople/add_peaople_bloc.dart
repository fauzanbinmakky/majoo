import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/addpeaople/add_people_event.dart';
import 'package:test_majoo/bloc/addpeaople/add_people_state.dart';
import 'package:test_majoo/repositiry/people_repo.dart';

class AddPeopleBloc extends Bloc<AddPeopleEvent, AddPeopleState> {
  PeopleRepoImp peopleRepoImp;

  AddPeopleBloc({required this.peopleRepoImp}) : super(AddPeopleInitial());

  @override
  Stream<AddPeopleState> mapEventToState(AddPeopleEvent event) async* {
    if (event is DoaddPeople) {
      yield* _doAddpeople(event);
    }
  }

  Stream<AddPeopleState> _doAddpeople(DoaddPeople event) async* {
    yield AddPeopleInitial();
    var data = await peopleRepoImp.AddPeopple(
        event.name,
        event.height,
        event.mass,
        event.hairColr,
        event.skinColor,
        event.eyeColor,
        event.brithyear,
        event.gender);
    if (data == "sukses") {
      yield AddPeopleSucses();
    } else {
      yield AddPeopleGagal();
    }
  }
}
