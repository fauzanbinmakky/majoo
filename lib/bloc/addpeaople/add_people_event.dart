import 'package:equatable/equatable.dart';

abstract class AddPeopleEvent extends Equatable {
  const AddPeopleEvent();
}

class DoaddPeople extends AddPeopleEvent {
  String name;
  String height;
  String mass;
  String hairColr;
  String skinColor;
  String eyeColor;
  String brithyear;
  String gender;

  @override
  List<Object?> get props =>
      [name, height, mass, hairColr, skinColor, eyeColor, brithyear, gender];

  DoaddPeople(this.name, this.height, this.mass, this.hairColr, this.skinColor,
      this.eyeColor, this.brithyear, this.gender);
}
