import 'package:equatable/equatable.dart';

abstract class FavoritState extends Equatable{

  const FavoritState();
}

class FavoritBerhasilDiganti extends FavoritState{
  @override
  List<Object?> get props => [];

}

class FavoritGagalDiganti extends FavoritState{
  @override
  List<Object?> get props => [];

}

class FavoritInital extends FavoritState {
  @override
  List<Object?> get props => [];

}