import 'package:equatable/equatable.dart';

abstract class SortState extends Equatable {
  const SortState();
}

class UrutanBerubah extends SortState {
  int posisi;

  @override
  List<Object?> get props => [posisi];

  UrutanBerubah(this.posisi);
}

class UrutanInitial extends SortState {
  @override
  List<Object?> get props => [];
}
