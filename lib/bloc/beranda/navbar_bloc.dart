import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/navbar_event.dart';

import 'navbar_state.dart';

class NavBarBloc extends Bloc<NavbarEvent, NavBarState> {
  NavBarBloc() : super(NavBarInitial());

  @override
  Stream<NavBarState> mapEventToState(NavbarEvent event) async* {
    if (event is GantiTab) {
      yield* _tappedNavbar(event);
    }
  }

  Stream<NavBarState> _tappedNavbar(GantiTab event) async* {
    yield PindahNavbar(event.posisi);
  }
}
