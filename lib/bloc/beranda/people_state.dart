import 'package:equatable/equatable.dart';
import 'package:test_majoo/local/local_people.dart';

abstract class PeoppleState extends Equatable {
  const PeoppleState();
}

class PeoppleSucsesList extends PeoppleState {
  List<LocalPeople?> list;

  @override
  // TODO: implement props
  List<Object?> get props => [list];

  PeoppleSucsesList(this.list);
}

class PeoppleSucsesGrid extends PeoppleState {
  List<LocalPeople?> list;

  @override
  // TODO: implement props
  List<Object?> get props => [list];

  PeoppleSucsesGrid(this.list);
}

class PeopleInital extends PeoppleState{
  @override

  List<Object?> get props => [];

}

class PeopleLoading extends PeoppleState{
  @override

  List<Object?> get props => [];

}

class PeopleKosong extends PeoppleState{
  @override

  List<Object?> get props => [];

}




