import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/view_event.dart';
import 'package:test_majoo/bloc/beranda/view_state.dart';

class ViewBloc extends Bloc<ViewEvent, ViewState> {
  ViewBloc() : super(ViewInitial());

  @override
  Stream<ViewState> mapEventToState(ViewEvent event) async* {
    if (event is GantiModelView) {
      yield* _ganitView(event);
    }
  }

  Stream<ViewState> _ganitView(GantiModelView event) async* {
    yield ViewBerubah(event.posisi);
  }
}
