import 'package:equatable/equatable.dart';

abstract class NavBarState extends Equatable {
  const NavBarState();
}

class PindahNavbar extends NavBarState {
  int posisi;

  @override
  List<Object?> get props => [posisi];

  PindahNavbar(this.posisi);
}

class NavBarInitial extends NavBarState {
  @override
  List<Object?> get props => [];

}
