import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/search_event.dart';
import 'package:test_majoo/bloc/beranda/search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial());

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is GantiKunciSearch) {
      yield* _dogantiKunci(event);
    }
  }

  Stream<SearchState> _dogantiKunci(GantiKunciSearch event) async* {
    yield SearchBerubah(event.kunci);
  }
}
