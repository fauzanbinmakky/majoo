import 'package:equatable/equatable.dart';

abstract class SearchState extends Equatable{
  const SearchState();
}

class SearchBerubah extends SearchState {
  String kunci;
  @override
  List<Object?> get props => [kunci];

  SearchBerubah(this.kunci);
}

class SearchInitial extends SearchState {
  @override
  List<Object?> get props => [];
}