import 'package:equatable/equatable.dart';

abstract class NavbarEvent extends Equatable{

  const NavbarEvent();
}

class GantiTab extends NavbarEvent{
  int posisi;
  @override
  List<Object?> get props => [posisi];

  GantiTab(this.posisi);
}