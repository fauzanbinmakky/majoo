import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/people_event.dart';
import 'package:test_majoo/bloc/beranda/people_state.dart';
import 'package:test_majoo/repositiry/people_repo.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeoppleState> {
  PeopleRepoImp peopleRepoImp;

  PeopleBloc({required this.peopleRepoImp}) : super(PeopleInital());

  @override
  Stream<PeoppleState> mapEventToState(PeopleEvent event) async* {
    if (event is getDataPeople) {
      yield* _getDataPeopple(event);
    } else if (event is getDataFavorit) {
      yield* _getDataFavoti(event);
    }
  }

  Stream<PeoppleState> _getDataPeopple(getDataPeople event) async* {

    late String tipe;
    if (event.kunci != "") {
      if (event.sortTyope == 0) {
        tipe = "asc";
      } else {
        tipe = "desc";
      }
      var data =
          await peopleRepoImp.getPeoppleSearch("%" + event.kunci + "%", tipe);
      if (data.isNotEmpty) {
        if (event.viewType == 0) {
          yield PeoppleSucsesList(data);
        } else {
          yield PeoppleSucsesGrid(data);
        }
      } else {
        yield PeopleKosong();
      }
    } else {
      if (event.sortTyope == 0) {
        tipe = "asc";
      } else {
        tipe = "desc";
      }
      var data = await peopleRepoImp.getLocalAllPeopleSort(tipe);
      if (data.isNotEmpty) {
        if (event.viewType == 0) {
          yield PeoppleSucsesList(data);
        } else {
          yield PeoppleSucsesGrid(data);
        }
      } else {
        yield PeopleKosong();
      }
    }
  }

  Stream<PeoppleState> _getDataFavoti(getDataFavorit event) async* {
    var data = await peopleRepoImp.getLocalFavoritPeople();
    if (data.isNotEmpty) {
      yield PeoppleSucsesList(data);
    } else {
      yield PeopleKosong();
    }
  }
}
