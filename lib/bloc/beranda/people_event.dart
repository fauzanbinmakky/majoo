import 'package:equatable/equatable.dart';

abstract class PeopleEvent extends Equatable {
  const PeopleEvent();
}

class getDataPeople extends PeopleEvent {
  int viewType;
  int sortTyope;
  String kunci;

  @override
  List<Object?> get props => [viewType, sortTyope, kunci];

  getDataPeople(this.viewType, this.sortTyope, this.kunci);
}

class getDataFavorit extends PeopleEvent {
  @override
  List<Object?> get props => [];
}
