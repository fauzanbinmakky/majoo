import 'package:equatable/equatable.dart';

abstract class ViewEvent extends Equatable {
  const ViewEvent();
}

class GantiModelView extends ViewEvent {
  int posisi;

  @override
  List<Object?> get props => [posisi];
  GantiModelView(this.posisi);
}
