import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable{
  const SearchEvent();
}

class GantiKunciSearch extends SearchEvent {
  String kunci;

  @override
  List<Object?> get props => [kunci];

  GantiKunciSearch(this.kunci);
}