import 'package:equatable/equatable.dart';

abstract class FavoritEvent extends Equatable {
  const FavoritEvent();
}

class DoGantiFavorit extends FavoritEvent {
  int id;
  String favorit;

  @override
  List<Object?> get props => [id,favorit];

  DoGantiFavorit(this.id, this.favorit);
}


