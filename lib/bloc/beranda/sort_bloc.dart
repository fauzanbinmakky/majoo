import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/sort_event.dart';
import 'package:test_majoo/bloc/beranda/sort_state.dart';

class SortBloc extends Bloc<SortEvent, SortState> {
  SortBloc() : super(UrutanInitial());

  @override
  Stream<SortState> mapEventToState(SortEvent event) async* {
    if (event is GantiUrutan) {
      yield* _gantiUrutan(event);
    }
  }

  Stream<SortState> _gantiUrutan(GantiUrutan event) async* {
    yield UrutanBerubah(event.posisi);
  }
}
