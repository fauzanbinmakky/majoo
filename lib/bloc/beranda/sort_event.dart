import 'package:equatable/equatable.dart';

abstract class SortEvent extends Equatable{

  const SortEvent();
}

class GantiUrutan extends SortEvent {
  int posisi;

  @override
  List<Object?> get props => [posisi];

  GantiUrutan(this.posisi);
}