import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/beranda/favorit_state.dart';
import 'package:test_majoo/bloc/beranda/favorite_event.dart';
import 'package:test_majoo/repositiry/people_repo.dart';

class FavoritBloc extends Bloc<FavoritEvent, FavoritState> {
  PeopleRepoImp peopleRepoImp;

  FavoritBloc({required this.peopleRepoImp}) : super(FavoritInital());

  @override
  Stream<FavoritState> mapEventToState(FavoritEvent event) async* {
    if (event is DoGantiFavorit) {
      yield* _dogantiFavorit(event);
    }
  }

  Stream<FavoritState> _dogantiFavorit(DoGantiFavorit event) async* {
    yield FavoritInital();
    var data =await peopleRepoImp.FavoritPeopple(event.id, event.favorit);
    if (data == "sukses") {
      yield FavoritBerhasilDiganti();
    } else {
      yield FavoritGagalDiganti();
    }
  }
}
