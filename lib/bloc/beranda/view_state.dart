import 'package:equatable/equatable.dart';

abstract class ViewState extends Equatable {
  const ViewState();
}

class ViewBerubah extends ViewState {
  int posisi;
  @override
  List<Object?> get props => [posisi];
  ViewBerubah(this.posisi);
}

class ViewInitial extends ViewState {
  @override
  List<Object?> get props => [];
}
