import 'package:equatable/equatable.dart';
import 'package:test_majoo/local/local_people.dart';

abstract class UbahPeopleEvent extends Equatable {
  const UbahPeopleEvent();
}

class DoaUbahData extends UbahPeopleEvent {
  int id;
  String name;
  String height;
  String mass;
  String hair_color;
  String skin_color;
  String eye_color;
  String birth_year;
  String gender;

  @override
  List<Object?> get props => [
        id,
        name,
        height,
        mass,
        hair_color,
        skin_color,
        eye_color,
        birth_year,
        gender
      ];

  DoaUbahData(this.id, this.name, this.height, this.mass, this.hair_color,
      this.skin_color, this.eye_color, this.birth_year, this.gender);
}

class DoHapusData extends UbahPeopleEvent {
  int id;

  @override
  List<Object?> get props => [id];

  DoHapusData(this.id);
}

class DoRefreshData extends UbahPeopleEvent {
  LocalPeople? localPeople;

  @override
  List<Object?> get props => [localPeople];

  DoRefreshData(this.localPeople);
}

class Toinitial extends UbahPeopleEvent {
  @override
  List<Object?> get props => [];

}
