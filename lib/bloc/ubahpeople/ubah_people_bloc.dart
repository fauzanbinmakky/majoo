import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_event.dart';
import 'package:test_majoo/bloc/ubahpeople/ubah_people_state.dart';
import 'package:test_majoo/repositiry/people_repo.dart';

class UbahPeopleBloc extends Bloc<UbahPeopleEvent, UbahPeopleState> {
  PeopleRepoImp peopleRepoImp;

  UbahPeopleBloc({required this.peopleRepoImp}) : super(UbahDataInitial());

  @override
  Stream<UbahPeopleState> mapEventToState(UbahPeopleEvent event) async* {
    if (event is DoaUbahData) {
      yield* _doUbahdata(event);
    } else if (event is DoHapusData) {
      yield* _doHapusData(event);
    } else if (event is DoRefreshData) {
      yield* _refReshData(event);
    }else if(event is Toinitial){
      yield* _toInital(event);
    }
  }

  Stream<UbahPeopleState> _doUbahdata(DoaUbahData event) async* {
    yield UbahDataInitial();
    var data = await peopleRepoImp.EditPeopple(
        event.id,
        event.name,
        event.height,
        event.mass,
        event.hair_color,
        event.skin_color,
        event.eye_color,
        event.birth_year,
        event.gender);
    if (data != null) {
      yield UbahDataSucses(data);
    } else {
      yield UbahDataGagal();
    }
  }

  Stream<UbahPeopleState> _doHapusData(DoHapusData event) async* {
    yield UbahDataInitial();
    var data = await peopleRepoImp.HapusPeopple(event.id);
    if (data == "sukses") {
      yield HapusDataSucses();
    } else {
      yield HapusDataGagal();
    }
  }

  Stream<UbahPeopleState> _refReshData(DoRefreshData event) async* {
    yield UbahDataInitial();
    if (event.localPeople != null) {
      yield RefreshData(event.localPeople);
    }
  }

  Stream<UbahPeopleState>_toInital(Toinitial event) async*{
    yield UbahDataInitial();
  }
}
