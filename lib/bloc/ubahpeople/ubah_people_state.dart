import 'package:equatable/equatable.dart';
import 'package:test_majoo/local/local_people.dart';

abstract class UbahPeopleState extends Equatable {
  const UbahPeopleState();
}

class UbahDataSucses extends UbahPeopleState {
  LocalPeople? localPeople;

  @override
  List<Object?> get props => [];

  UbahDataSucses(this.localPeople);
}

class UbahDataGagal extends UbahPeopleState {
  @override
  List<Object?> get props => [];
}

class HapusDataGagal extends UbahPeopleState {
  @override
  List<Object?> get props => [];
}

class HapusDataSucses extends UbahPeopleState {
  @override
  List<Object?> get props => [];
}

class UbahDataInitial extends UbahPeopleState {
  @override
  List<Object?> get props => [];
}

class RefreshData extends UbahPeopleState {
  LocalPeople? localPeople;
  @override
  List<Object?> get props => [localPeople];

  RefreshData(this.localPeople);
}
