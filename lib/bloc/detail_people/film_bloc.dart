import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/bloc/detail_people/film_event.dart';
import 'package:test_majoo/bloc/detail_people/film_state.dart';
import 'package:test_majoo/repositiry/film_repo.dart';

class FilmBloc extends Bloc<FilmEvent, FilmState> {
  FilmRepoImp repoImp;

  FilmBloc({required this.repoImp}) : super(FilmInitial());

  @override
  Stream<FilmState> mapEventToState(FilmEvent event) async* {
    if (event is DoGetFil) {
      yield* _getFilm(event);
    }
  }

  Stream<FilmState> _getFilm(DoGetFil event) async* {
    yield FilmInitial();
    var data = await repoImp.getFilm(event.idpeople);
    if (data.isNotEmpty) {
      yield FilmAda(data);
    } else {
      yield FilmKososng();
    }
  }
}
