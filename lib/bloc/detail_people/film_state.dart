import 'package:equatable/equatable.dart';
import 'package:test_majoo/local/local_film.dart';

abstract class FilmState extends Equatable {
  const FilmState();
}

class FilmInitial extends FilmState {
  @override
  List<Object?> get props => [];
}

class FilmAda extends FilmState {
  List<LocalFilm?> list;

  @override
  List<Object?> get props => [list];

  FilmAda(this.list);
}

class FilmKososng extends FilmState {
  @override
  List<Object?> get props => [];
}
