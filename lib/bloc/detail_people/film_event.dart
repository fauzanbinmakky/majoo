import 'package:equatable/equatable.dart';

abstract class FilmEvent extends Equatable {
  const FilmEvent();
}

class DoGetFil extends FilmEvent {
  int idpeople;

  @override
  // TODO: implement props
  List<Object?> get props => [idpeople];

  DoGetFil(this.idpeople);
}
