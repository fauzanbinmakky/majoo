import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/bloc/auth/auth_event.dart';
import 'package:test_majoo/bloc/auth/auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(InitialAuth());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is CekAuth) {
      yield* _cekAut(event);
    }
  }

  Stream<AuthState> _cekAut(CekAuth event) async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = await prefs.getString("token");
    String? data = await prefs.getString("data");
    if (token != null && token != ""&& data!=null &&data=="ada") {
      yield Authenticated();
    } else {
      yield UnAuthenticated();
    }
  }
}
